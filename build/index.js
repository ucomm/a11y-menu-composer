/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/a11y-menu/src/js/Navigation/Navigation.js":
/*!****************************************************************!*\
  !*** ./node_modules/a11y-menu/src/js/Navigation/Navigation.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Navigation = function () {
  function Navigation() {
    var opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, Navigation);

    this.menu = null;
    this.hasNestedSubmenu = false;
    this.opts = opts;
    this.fontFamilies = ['FontAwesome', 'Font Awesome 5 Free', 'Glyphicons Halflings'];
  }

  _createClass(Navigation, [{
    key: 'chevronSwitcher',
    value: function chevronSwitcher(element) {
      if (element.localName !== "button") return;

      var icon = element.children[0];
      var _opts = this.opts,
          chevronDown = _opts.chevronDown,
          chevronUp = _opts.chevronUp;

      element.getAttribute('aria-expanded') === 'true' ? icon.setAttribute('data-before', chevronUp) : icon.setAttribute('data-before', chevronDown);
    }
  }, {
    key: 'clickHandler',
    value: function clickHandler(evt) {
      var target = evt.target;
      var submenuList = target.nextSibling;
      // find out if there is a nested submenu inside a top level item
      submenuList.getElementsByTagName('ul').length ? this.hasNestedSubmenu = true : this.hasNestedSubmenu = false;
      // if something weird happens, don't allow any further event handling.
      if (!target.getAttribute('aria-haspopup')) return;

      // if we're on a list item that is really just a toggle, 
      // that is it doesn't have a page that it goes to, prevent the page from reloading.
      target.className === 'submenu-toggle' ? evt.preventDefault() : null;

      submenuList.classList.toggle('submenu-list-open');
      target.setAttribute('aria-expanded', 'true');

      submenuList.classList.contains('submenu-list-open') ? null : target.setAttribute('aria-expanded', 'false');

      if (target.children) {
        this.chevronSwitcher(target);
      }
    }
  }, {
    key: 'focusInHandler',
    value: function focusInHandler(evt) {
      var target = evt.target,
          relatedTarget = evt.relatedTarget;
      var parentNode = target.parentNode,
          offsetParent = target.offsetParent;

      var parentUL = offsetParent.parentNode;

      // if the parentUL isn't the menu and it contains the target return
      if (parentUL !== this.menu && parentUL.contains(target)) {
        return;
      } else {
        // close the submenu when you leave
        var expandedElementCollection = parentUL.querySelectorAll('[aria-expanded="true"]');
        var openElementCollection = parentUL.getElementsByClassName('submenu-list-open');

        if (expandedElementCollection.length) {
          expandedElementCollection[0].setAttribute('aria-expanded', 'false');
          openElementCollection[0].classList.remove('submenu-list-open');
          this.chevronSwitcher(expandedElementCollection[0]);
        }
      }
    }
  }, {
    key: 'hoverHandler',
    value: function hoverHandler(evt) {
      var type = evt.type,
          target = evt.target;

      if (type === 'mouseout' && target.getAttribute('aria-haspopup') === "true") {
        target.setAttribute('aria-expanded', 'false');
      } else if (type === 'mouseover' && target.getAttribute('aria-haspopup') === "false") {
        target.setAttribute('aria-expanded', 'true');
      }

      // if you hover and the htmlcollection length is greater than 0
      if (target.children.length > 0) {
        this.chevronSwitcher(target);
      }
    }
  }, {
    key: 'eventDispatcher',
    value: function eventDispatcher(evt) {
      // dispatch event listeners to the correct functions.
      switch (evt.type) {
        case 'click':
          this.clickHandler(evt);
          break;
        case 'focusin':
          this.focusInHandler(evt);
          break;
        case 'mouseover':
        case 'mouseout':
          this.hoverHandler(evt);
          break;
        default:
          return;
          break;
      }
    }
  }, {
    key: 'setEventListeners',
    value: function setEventListeners() {
      var _this = this;

      // if this script is running, remove the 'no-js' class from the elements.
      var listElements = Array.prototype.slice.call(this.menu.getElementsByClassName('no-js'));
      listElements.forEach(function (element) {
        element.classList.remove('no-js');
      });
      // define a list of possible event listeners
      var listeners = ['click', 'focusin', 'mouseout', 'mouseover'];
      // attach them to the menu.
      for (var i = 0; i < listeners.length; i++) {
        this.menu.addEventListener(listeners[i], function (evt) {
          // dispatch the events to the class methods.
          _this.eventDispatcher(evt);
        });
      }
    }
  }, {
    key: 'setSubmenuIcon',
    value: function setSubmenuIcon() {
      var _this2 = this;

      // possible font-family for the icons
      var fontFamily = this.opts.fontFamily;

      if (!this.fontFamilies.includes(fontFamily)) {
        fontFamily = '';
      }

      // the list of all the submenu icons
      var icons = this.menu.querySelectorAll('.submenu-icon');
      // the css to inject into the page
      var hoverCss = '\n      nav ul li span::before {\n        content: \'' + this.opts.chevronDown + '\';\n        font-family: \'' + fontFamily + '\';\n        font-weight: bold;\n      }\n      nav ul li:hover > button span::before,\n      nav ul li:focus > button span::before { \n        content: \'' + this.opts.chevronUp + '\';\n        font-family: \'' + fontFamily + '\'; \n        font-weight: bold;\n      }';

      // create a style tag
      var style = document.createElement('style');
      // add the styles to the tag (or a stylesheet if it exists)
      if (style.styleSheet) {
        style.styleSheet.cssText = hoverCss;
      } else {
        style.appendChild(document.createTextNode(hoverCss));
      }
      // add the tag to the <head>
      document.getElementsByTagName('head')[0].appendChild(style);
      // set the data-before attribute to the values passed in the constructor.
      icons.forEach(function (icon) {
        icon.setAttribute('data-before', _this2.opts.chevronDown);
      });
    }
  }, {
    key: 'init',
    value: function init(menuElement) {
      this.menu = menuElement;
      this.setEventListeners();
      this.setSubmenuIcon();
    }
  }]);

  return Navigation;
}();

module.exports = Navigation;

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _a11yMenu = __webpack_require__(/*! a11y-menu */ "./node_modules/a11y-menu/src/js/Navigation/Navigation.js");

var _a11yMenu2 = _interopRequireDefault(_a11yMenu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./src/sass/main.scss":
/*!****************************!*\
  !*** ./src/sass/main.scss ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!****************************************************!*\
  !*** multi ./src/js/index.js ./src/sass/main.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./src/js/index.js */"./src/js/index.js");
module.exports = __webpack_require__(/*! ./src/sass/main.scss */"./src/sass/main.scss");


/***/ })

/******/ });
//# sourceMappingURL=index.js.map